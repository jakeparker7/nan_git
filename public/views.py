from django.shortcuts import render

#import models

def index(request):
    """View function for home page of site."""
    #create counts
    #and init models
    context = {}
    # Render the HTML template index.html with the data in the context variable
    return render(request, 'index.html', context=context)
